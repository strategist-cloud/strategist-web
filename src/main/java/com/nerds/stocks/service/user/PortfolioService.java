package com.nerds.stocks.service.user;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.cache.SymbolsCache;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Portfolio;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.ResourceException;
import com.nerds.stocks.db.entities.PortfolioEntity;
import com.nerds.stocks.db.entities.QuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;
import com.nerds.stocks.db.entities.TransactionEntity;
import com.nerds.stocks.db.entities.UserEntity;
import com.nerds.stocks.db.repos.PortfolioRepository;
import com.nerds.stocks.db.repos.QuotesRepository;
import com.nerds.stocks.db.repos.StocksRepository;
import com.nerds.stocks.db.repos.TransactionsRepository;
import com.nerds.stocks.db.repos.UsersRepository;
import com.nerds.stocks.interfaces.DataCollectorServiceInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.Getter;
import lombok.Setter;

@Service
@ConfigurationProperties(prefix = "stocks")
@Getter
@Setter
public class PortfolioService {

    String defaultTickers;

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    StocksRepository stocksRepository;
    @Autowired
    QuotesRepository quotesRepo;
    @Autowired
    PortfolioRepository portfoliosRepo;
    @Autowired
    TransactionsRepository transactionsRepository;
    @Autowired
    DataCollectorServiceInterface dataCollectorService;
    @Autowired
    SymbolsCache symbolsCache;

    public List<Portfolio> getWatchList(String userName) {
        List<PortfolioEntity> portfolios = new ArrayList<PortfolioEntity>();

        List<UserEntity> obj = usersRepository.findByUserName(userName);
        UserEntity user = null;
        if (CollectionUtils.isEmpty(obj)) {
            logger.debug("Unable to find user " + userName + " in database....., Initiating a new portfolio");
            user = new UserEntity();
            user.setUserName(userName);

            user = initWatchlist(user);
            portfolios = user.getPortfolios();
        } else {
            user = obj.get(0);
            logger.debug("Found User in the database " + user);
            portfolios = user.getPortfolios();
            if (CollectionUtils.isEmpty(portfolios)) {
                user = initWatchlist(user);
                portfolios = user.getPortfolios();
            }
        }

        return portfolios.parallelStream().map(portfolio -> new Portfolio(portfolio)).collect(Collectors.toList());

    }

    public UserEntity initWatchlist(UserEntity user) {
        PortfolioEntity watchlist = createPortfolioEntity(user, "Watchlist");
        user.getPortfolios().add(watchlist);
        return usersRepository.saveAndFlush(user);
    }

    public PortfolioEntity createPortfolioEntity(String userName, String portfolioName) {
        List<UserEntity> users = usersRepository.findByUserName(userName);
        UserEntity user = null;
        if (CollectionUtils.isEmpty(users))
            user = users.get(0);

        return createPortfolioEntity(user, portfolioName);
    }

    public PortfolioEntity createPortfolioEntity(UserEntity user, String portfolioName) {
        PortfolioEntity watchList = new PortfolioEntity();
        watchList.setPortfolioName(portfolioName);
        watchList.setUser(user);
        watchList.getHoldings()
                .addAll(Arrays.stream(defaultTickers.split(",")).map(exchangeTickerMap -> exchangeTickerMap.split(":"))
                        .filter(exchangeTickerMap -> exchangeTickerMap.length == 2)
                        .map(exchangeTickerMap -> createTransactionEntity(user, watchList, exchangeTickerMap[0],
                                exchangeTickerMap[1]))
                        .collect(Collectors.toList()));
        return watchList;
    }

    public TransactionEntity createTransactionEntity(UserEntity user, PortfolioEntity portfolio, String exchange,
            String ticker) {
        Timestamp currentTime = new Timestamp(new Date().getTime());
        TransactionEntity entity = new TransactionEntity();
        entity.setCount(0);
        entity.setOrderTime(currentTime);
        entity.setPortfolio(portfolio);
        StockEntity stock = stocksRepository.get(exchange, ticker);
        if (stock == null) {

            Fundamentals stockInfo = dataCollectorService.initStock(exchange, ticker);
            if (stockInfo != null) {
                stock = stockInfo.toStockEntity();
            } else {
                throw new ResourceException(HttpStatus.OK, "No Info found for " + ticker);
            }

            // stock = initializer.initStock(ticker, null);
            // sent null to let the initializer get the latest quote from external service
            // provider
        }

        entity.setStock(stock);
        entity.setUser(user);
        List<QuoteEntity> records = quotesRepo.getPrice(exchange, ticker);
        if (!CollectionUtils.isEmpty(records)) {
            entity.setPrice(Quote.getQuote(records.get(0)).getClosePrice());
        }
        entity.setTotal(0);
        return entity;
    }

    public Portfolio addTicker(String userName, String portfolioName, String exchange, String ticker) {
        logger.debug("Trying to find portfolio " + portfolioName + " for " + userName + " user to add " + ticker
                + " symbol ");
        PortfolioEntity entity = portfoliosRepo.findByNameAndUser(portfolioName, userName);
        if (entity == null) {
            logger.error("Portfolio " + portfolioName + " NOT FOUND for user " + userName);
            throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Portfolio " + portfolioName + " not found for user " + userName);
        } else {
            logger.info("Found " + portfolioName + " porfolio for user " + userName);
            entity.getHoldings().add(createTransactionEntity(entity.getUser(), entity, exchange, ticker));
            portfoliosRepo.saveAndFlush(entity);
        }
        return new Portfolio(entity);
    }

    public Portfolio removeTicker(String userName, String portfolioName, String exchangeId, String ticker) {
        logger.debug("Trying to find portfolio " + portfolioName + " for " + userName + " user to remove " + ticker
                + " symbol ");
        PortfolioEntity entity = portfoliosRepo.findByNameAndUser(portfolioName, userName);
        if (entity == null) {
            logger.error("Portfolio " + portfolioName + " NOT FOUND for user " + userName);
            throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Portfolio " + portfolioName + " not found for user " + userName);
        } else {
            logger.info("Found " + portfolioName + " porfolio for user " + userName + " and removing " + ticker);
            transactionsRepository.remove(portfolioName, userName, exchangeId, ticker);
            transactionsRepository.flush();
        }
        return new Portfolio(portfoliosRepo.findByNameAndUser(portfolioName, userName));
    }

    public String getExchangeForTicker(String ticker) {
        String exchange = null;

        return exchange;
    }

    static final Logger logger = LoggerFactory.getLogger(PortfolioService.class);
}