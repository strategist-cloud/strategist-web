package com.nerds.stocks.service.price;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.db.entities.QuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;
import com.nerds.stocks.db.repos.DailyQuotesRepository;
import com.nerds.stocks.db.repos.FiveMinQuotesRepository;
import com.nerds.stocks.db.repos.QuotesRepository;
import com.nerds.stocks.db.repos.StocksRepository;
import com.nerds.stocks.db.repos.ThirtyMinQuotesRepository;
import com.nerds.stocks.interfaces.DataCollectorServiceInterface;
import com.nerds.stocks.service.cache.ExchangeCache;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.service.data.TIMESERIES;
import com.nerds.stocks.service.util.TimeUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class StockServiceImpl implements StockService {

    @Autowired
    QuotesRepository currentQuotes;
    @Autowired
    DailyQuotesRepository dailyQuotes;
    @Autowired
    FiveMinQuotesRepository fiveMinQuotes;
    @Autowired
    ThirtyMinQuotesRepository thirtyMinQuotes;
    @Autowired
    StocksRepository stocksRepository;
    @Autowired
    DataCollectorServiceInterface dataCollectorService;
    @Autowired
    ExchangeCache exchangesCache;

    @Override
    public List<Quote> getPrices(Exchange exchange, String ticker, TIMESERIES timeSeries) {
        List<Quote> quotes = new ArrayList<Quote>();
        List<QuoteEntity> quotesInDB = new ArrayList<QuoteEntity>();
        Timestamp to = new Timestamp(new Date().getTime());
        try {
            Calendar time = TimeUtil.getTimestamp(timeSeries, exchange);
            INTERVAL interval = TimeUtil.getInterval(timeSeries);

            Timestamp from = new Timestamp(time.getTimeInMillis());
            if (interval == INTERVAL._5MIN) {
                quotesInDB.addAll(fiveMinQuotes.getQuotesFor(ticker, from, to));
            } else if (interval == INTERVAL._30MIN) {
                quotesInDB.addAll(thirtyMinQuotes.getQuotesFor(ticker, from, to));
            } else {
                quotesInDB.addAll(dailyQuotes.getQuotesFor(ticker, from, to));
            }
            quotes = Quote.getQuoteList(quotesInDB);
        } catch (Exception e) {
            logger.error("Error while trying to get prices from DB", e);
        }
        return quotes;
    }

    @Override
    public Fundamentals getFundamentals(Exchange exchange, String ticker) {
        if (exchange == null) {
            exchange = exchangesCache.getExchange("NYSE");
        }

        Fundamentals stockInfo = null;
        StockEntity stock = stocksRepository.get(exchange.getExchangeId(), ticker);
        if (stock != null) {
            stockInfo = new Fundamentals(stock);
        }

        return stockInfo;
    }

    @Override
    public Quote getCurrentPrice(Exchange exchange, String ticker) {
        List<QuoteEntity> records = currentQuotes.getPrice(exchange.getExchangeId(), ticker);
        if (CollectionUtils.isEmpty(records)) {
            return null;
        } else {
            return Quote.getQuote(records.get(0));
        }
    }

    @Override
    public Quote getPrice(Exchange exchange, String ticker, int time) {
        Calendar givenDate = Calendar.getInstance();
        givenDate.setTimeInMillis(time);
        List<QuoteEntity> quotes = new ArrayList<QuoteEntity>();
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, -1);
        if (givenDate.compareTo(date) > 0) {
            quotes.addAll(fiveMinQuotes.getQuotesFrom(ticker, new Timestamp(time)));
        } else {
            date.add(Calendar.DATE, -4);
            if (givenDate.compareTo(date) > 0) {
                quotes.addAll(thirtyMinQuotes.getQuotesFrom(ticker, new Timestamp(time)));
            } else {
                quotes.addAll(dailyQuotes.getQuotesFrom(ticker, new Timestamp(time)));
            }
        }

        if (quotes == null || quotes.isEmpty()) {
            return null;
        }
        return Quote.getQuote(quotes.get(0));
    }

    static final Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);
}