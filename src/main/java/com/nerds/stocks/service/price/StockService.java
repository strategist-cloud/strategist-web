package com.nerds.stocks.service.price;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.service.data.TIMESERIES;

@Component
public interface StockService {
    List<Quote> getPrices(Exchange exchange, String ticker, TIMESERIES timeSeries);

    Fundamentals getFundamentals(Exchange exchange, String ticker);

    Quote getCurrentPrice(Exchange exchange, String ticker);

    Quote getPrice(Exchange exchange, String ticker, int time);
}