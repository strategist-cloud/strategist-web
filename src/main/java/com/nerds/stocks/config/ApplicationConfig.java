package com.nerds.stocks.config;

import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "stocks")
public class ApplicationConfig {

    List<String> allowedOrigins;

    @PostConstruct
    void setDefaultTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    // @Bean
    // public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
    // UrlBasedCorsConfigurationSource source = new
    // UrlBasedCorsConfigurationSource();
    // CorsConfiguration config = new CorsConfiguration();
    // config.setAllowCredentials(true);
    // config.setAllowedOrigins(allowedOrigins);
    // config.setAllowedMethods(Collections.singletonList("*"));
    // config.setAllowedHeaders(Collections.singletonList("*"));
    // source.registerCorsConfiguration("/**", config);
    // FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new
    // CorsFilter(source));
    // bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
    // return bean;
    // }

    public List<String> getAllowedOrigins() {
        return allowedOrigins;
    }

    public void setAllowedOrigins(List<String> allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }
}