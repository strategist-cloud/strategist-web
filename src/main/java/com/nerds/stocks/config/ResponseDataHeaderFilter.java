package com.nerds.stocks.config;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nerds.stocks.data.ResponseHeader;
import com.nerds.stocks.data.ResponseStatus;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ResponseDataHeaderFilter extends OncePerRequestFilter {

    public static final String RESPONSE_HEADER_ATTR_NAME = "RESPONSE_DATA_HEADER";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (isAsyncDispatch(request)) {
            filterChain.doFilter(request, response);
        } else {
            createRequestHeader(request, response, filterChain);
        }
    }

    protected void createRequestHeader(HttpServletRequest request, HttpServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        ResponseHeader responseHeader = new ResponseHeader();
        try {
            log.info("Initiating request header");
            // responseHeader.setEnvironment(environment);
            responseHeader.setRequestTime(LocalDateTime.now());
            request.setAttribute(RESPONSE_HEADER_ATTR_NAME, responseHeader);
            filterChain.doFilter(request, response);
            responseHeader.setResponseTime(LocalDateTime.now());
            responseHeader.setResult(ResponseStatus.SUCCESSFUL);
        } finally {
            log.info("Finalizing request header");
            responseHeader.setRequestTime(LocalDateTime.now());
        }
    }

}