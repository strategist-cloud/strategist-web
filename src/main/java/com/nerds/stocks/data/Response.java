package com.nerds.stocks.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response<T> {
    ResponseHeader header;
    T body;

    public Response(T body, ResponseHeader header) {
        this.body = body;
        this.header = header;
    }
}