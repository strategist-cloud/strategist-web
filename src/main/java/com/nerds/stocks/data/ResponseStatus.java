package com.nerds.stocks.data;

public enum ResponseStatus {
    SUCCESSFUL(0, "Successful"), PROGRAM_ERROR(1, "PROGRAM ERROR"), VALIDATION_ERROR(2, "Invalid parameters");

    int code;
    String message;

    ResponseStatus(int statusCode, String statusMessage) {
        this.code = statusCode;
        this.message = statusMessage;
    }
}