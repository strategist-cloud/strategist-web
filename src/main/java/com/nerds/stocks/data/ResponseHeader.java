package com.nerds.stocks.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseHeader {
    LocalDateTime requestTime;
    LocalDateTime responseTime;
    String environment;
    List<String> errors;
    List<String> warnings;

    ResponseStatus result;

    public void addError(String error) {
        if (CollectionUtils.isEmpty(errors)) {
            errors = new ArrayList<String>();
        }

        errors.add(error);
    }

    public void addWarning(String warning) {
        if (CollectionUtils.isEmpty(warnings)) {
            warnings = new ArrayList<String>();
        }

        errors.add(warning);
    }
}
