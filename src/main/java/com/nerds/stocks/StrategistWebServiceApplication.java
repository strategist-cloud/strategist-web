package com.nerds.stocks;

import com.nerds.stocks.config.FeignClientInterceptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import feign.RequestInterceptor;

@SpringBootApplication
@EnableFeignClients
@EnableAutoConfiguration
@EnableOAuth2Sso
public class StrategistWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistWebServiceApplication.class, args);
	}

	@Bean
	public RequestInterceptor getUserFeignClientInterceptor() {
		return new FeignClientInterceptor();
	}
}
