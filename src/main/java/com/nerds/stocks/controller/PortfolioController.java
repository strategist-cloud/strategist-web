package com.nerds.stocks.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.nerds.stocks.config.ResponseDataHeaderFilter;
import com.nerds.stocks.data.Portfolio;
import com.nerds.stocks.data.Response;
import com.nerds.stocks.data.ResponseHeader;
import com.nerds.stocks.service.user.PortfolioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/portfolio")
public class PortfolioController {

        @Autowired
        PortfolioService portfolioService;

        @RequestMapping(path = "/getAll")
        public Response<List<Portfolio>> getWatchList(Principal principal, HttpServletRequest request) {
                String userName = principal.getName();
                logger.info("Get Watchlist: Found User id in request " + userName);
                List<Portfolio> portfolios = portfolioService.getWatchList(userName);
                ResponseHeader responseHeader = (ResponseHeader) request
                                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
                return new Response<List<Portfolio>>(portfolios, responseHeader);
        }

        @RequestMapping(path = "/addTicker")
        public Response<Portfolio> addStockToPortfolio(Principal principal, String portfolioName, String exchange,
                        String ticker, HttpServletRequest request) {
                ResponseHeader responseHeader = (ResponseHeader) request
                                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
                return new Response<Portfolio>(
                                portfolioService.addTicker(principal.getName(), portfolioName, exchange, ticker),
                                responseHeader);
        }

        @RequestMapping(path = "/removeTicker")
        public Response<Portfolio> removeStockToPortfolio(Principal principal, String portfolioName, String exchange,
                        String ticker, HttpServletRequest request) {
                ResponseHeader responseHeader = (ResponseHeader) request
                                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
                return new Response<Portfolio>(
                                portfolioService.removeTicker(principal.getName(), portfolioName, exchange, ticker),
                                responseHeader);
        }

        @RequestMapping(path = "/add/{portfolioName}")
        public Response<List<Portfolio>> addPortfolio(Principal principal, String portfolioName,
                        HttpServletRequest request) {
                portfolioService.createPortfolioEntity(principal.getName(), portfolioName);
                List<Portfolio> portfolios = portfolioService.getWatchList(principal.getName());
                ResponseHeader responseHeader = (ResponseHeader) request
                                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);

                return new Response<List<Portfolio>>(portfolios, responseHeader);
        }

        static final Logger logger = LoggerFactory.getLogger(PortfolioController.class);
}