package com.nerds.stocks.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.nerds.stocks.config.ResponseDataHeaderFilter;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.Response;
import com.nerds.stocks.data.ResponseHeader;
import com.nerds.stocks.service.cache.ExchangeCache;
import com.nerds.stocks.service.data.TIMESERIES;
import com.nerds.stocks.service.price.StockService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/stocks")
public class StockController {

    @Autowired
    StockService service;

    @Autowired
    ExchangeCache exchangesCache;

    static String symbolsInJsonFormat = "[]"; // return an empty array response to user if no companies are already
                                              // defined

    public StockController() {
        logger.info("****************** Trying to load all the symbols from static json file");
        initUsTickers();
    }

    public static String initUsTickers() {
        final Resource resource = new ClassPathResource("data/us_symbols.json");
        try {
            symbolsInJsonFormat = new String(Files.readAllBytes(resource.getFile().toPath()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.error("Error occurred while trying to load all the symbols from json file");
        }
        return symbolsInJsonFormat;
    }

    @RequestMapping(path = "/get/{exchange}/{ticker}")
    public Response<Fundamentals> getStockInfo(@PathVariable String exchange, @PathVariable String ticker,
            HttpServletRequest request) {
        ResponseHeader responseHeader = (ResponseHeader) request
                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
        return new Response<Fundamentals>(service.getFundamentals(exchangesCache.getExchange(exchange), ticker),
                responseHeader);
    }

    @RequestMapping(path = "/getAllSymbols")
    public Response<String> getAllSymbols(HttpServletRequest request) {
        ResponseHeader responseHeader = (ResponseHeader) request
                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
        return new Response<String>(symbolsInJsonFormat, responseHeader);
    }

    @RequestMapping(path = "/getQuotes/{exchange}/{ticker}")
    public Response<List<Quote>> getQuotes(@PathVariable String exchange, @PathVariable String ticker,
            @RequestParam TIMESERIES timeseries, HttpServletRequest request) {
        ResponseHeader responseHeader = (ResponseHeader) request
                .getAttribute(ResponseDataHeaderFilter.RESPONSE_HEADER_ATTR_NAME);
        return new Response<List<Quote>>(service.getPrices(exchangesCache.getExchange(exchange), ticker, timeseries),
                responseHeader);
    }

    static final Logger logger = LoggerFactory.getLogger(StockController.class);
}