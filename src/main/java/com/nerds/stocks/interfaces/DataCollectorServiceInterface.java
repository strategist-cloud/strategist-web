package com.nerds.stocks.interfaces;

import com.nerds.stocks.data.Fundamentals;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties.FeignClientConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "stocks-data-collector-service", configuration = FeignClientConfiguration.class)
public interface DataCollectorServiceInterface {

	@RequestMapping("/dataCollectorService/initStock/{exchange}/{ticker}")
	public Fundamentals initStock(@PathVariable(value = "exchange") String exchangeId,
			@PathVariable("ticker") String ticker);
}
