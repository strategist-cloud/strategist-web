package com.nerds.stocks.cache;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.db.entities.CompanyEntity;
import com.nerds.stocks.service.cache.ExchangeCache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class SymbolsCache {
    @Autowired
    ExchangeCache exchangeCache;

    List<CompanyEntity> companies = new ArrayList<CompanyEntity>();

    public static List<CompanyEntity> getUsCompanies() {
        ObjectMapper mapper = new ObjectMapper();
        final Resource resource = new ClassPathResource("data/us_trading_companies.json");
        List<CompanyEntity> companies = new ArrayList<CompanyEntity>();
        try {
            String symbolsInJsonFormat = new String(Files.readAllBytes(resource.getFile().toPath()),
                    StandardCharsets.UTF_8);
            companies = mapper.readValue(symbolsInJsonFormat, new TypeReference<List<CompanyEntity>>() {
            });
        } catch (Exception e) {
            logger.error("Error while parsing companies info from json ", e);
        }
        return companies;
    }

    public Exchange getExchange(String ticker) {
        Exchange exchange = null;
        CompanyEntity companyForTicker = companies.parallelStream()
                .filter(company -> ticker.equalsIgnoreCase(company.getTicker())).findFirst().orElse(null);

        if (companyForTicker != null) {
            exchange = exchangeCache.getExchange(companyForTicker.getExchange());
        }

        return exchange;
    }

    static final Logger logger = LoggerFactory.getLogger(SymbolsCache.class);
}